shader_type canvas_item;

uniform sampler2D last;

void fragment() {
	vec2 uv = SCREEN_UV - vec2(0.5, 0.5);
	uv.x *= (sin(SCREEN_UV.y + 1.0) / 1.5);
	uv.y *= (sin(SCREEN_UV.x + 1.0) / 1.5);
	uv += vec2(0.5, 0.5);
	vec3 blurred = textureLod(SCREEN_TEXTURE, uv, 3.0).rgb;
	float greyscale_blurred = (blurred.r + blurred.g + blurred.b) / 3.0;
	blurred.r = (blurred.r + (greyscale_blurred / 1.5)) / 3.0;
	blurred.g = (blurred.g + (greyscale_blurred * 2.0)) / 3.0;
	blurred.b = (blurred.b + (greyscale_blurred)) / 3.0;
	vec3 sharp = textureLod(SCREEN_TEXTURE, uv, 1.0).rgb;
	float greyscale = (sharp.r + sharp.g + sharp.b) / 3.0;
	sharp.r = greyscale;
	sharp.g = greyscale;
	sharp.b = greyscale;
	vec2 uv_zoom = uv - vec2(0.5, 0.5);
	uv_zoom.x /= 2.0;
	uv_zoom.y /= 2.0;
	uv_zoom += vec2(0.5, 0.5);
	vec3 zoom = textureLod(SCREEN_TEXTURE, uv_zoom, 4.0).rgb;
	vec3 final = sharp + blurred + zoom;
	COLOR.rgb = final;
}