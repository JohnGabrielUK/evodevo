shader_type canvas_item;

uniform sampler2D last;

void fragment() {
	vec3 bloom = textureLod(SCREEN_TEXTURE, SCREEN_UV, 5.0).rgb / 2.0;
	vec3 blurred = textureLod(SCREEN_TEXTURE, SCREEN_UV, 3.0).rgb;
	vec3 sharp = textureLod(SCREEN_TEXTURE, SCREEN_UV, 1.0).rgb;
	vec3 final = sharp + blurred + bloom;
	COLOR.rgb = final;
}