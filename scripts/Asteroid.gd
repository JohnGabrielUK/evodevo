extends "res://scripts/Enemy.gd"

const obj_explosion = preload("res://objects/explosions/BigExplosion.tscn")
onready var damage = 3

func _ready():
	._ready()
	# Set up parent variables
	audio_hit = $Audio_Hit
	hitpoints = 5

func _process(delta):
	._process(delta) # Call parent function
	position.y += delta * 20
	if position.y > 200:
		queue_free()

func die():
	var explosion = obj_explosion.instance()
	explosion.global_position = position
	get_parent().add_child(explosion)
	queue_free()
