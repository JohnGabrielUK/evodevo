extends Area2D

onready var sprite = $AnimatedSprite

var damage
var velocity

func flip(value):
	sprite.flip_h = value

func set_size(value):
	scale.x = value
	scale.y = value

func _process(delta):
	position.x += velocity.x * delta
	position.y += velocity.y * delta
