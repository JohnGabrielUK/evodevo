extends Node

const SAVE_PATH = "user://settings.cfg"

var config

func set_audio_levels():
	var volume_bgm = get_volume_bgm()
	var volume_sfx = get_volume_sfx()
	var volume_ui = get_volume_ui()
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("BGM"), linear2db(volume_bgm))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SFX"), linear2db(volume_sfx))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("UI"), linear2db(volume_ui))

func set_controls():
	for next_action in ["move_up", "move_down", "move_left", "move_right", "fire", "pause"]:
		var key = get_key_binding(next_action)
		bind_action(next_action, key)

func bind_action(action, key):
	var event = InputEventKey.new()
	event.scancode = key
	# Remove any old bindings
	for old_event in InputMap.get_action_list(action):
		InputMap.action_erase_event(action, old_event)
	# Add the new one
	InputMap.action_add_event(action, event)

func set_volume_bgm(value):
	config.set_value("volume", "bgm", value)
	set_audio_levels()

func set_volume_sfx(value):
	config.set_value("volume", "sfx", value)
	set_audio_levels()

func set_volume_ui(value):
	config.set_value("volume", "ui", value)
	set_audio_levels()

func set_camera_shake(value):
	config.set_value("graphics", "camera_shake", value)

func set_use_shaders(value):
	config.set_value("graphics", "use_shaders", value)

func set_key_binding(action, key):
	config.set_value("controls", action, key)
	bind_action(action, key)

func get_volume_bgm():
	return config.get_value("volume", "bgm", 1.0)

func get_volume_sfx():
	return config.get_value("volume", "sfx", 1.0)

func get_volume_ui():
	return config.get_value("volume", "ui", 1.0)

func get_camera_shake():
	return config.get_value("graphics", "camera_shake", true)

func get_use_shaders():
	return config.get_value("graphics", "use_shaders", true)

func get_key_binding(action):
	var default = InputMap.get_action_list(action)[0].scancode
	return config.get_value("controls", action, default)

func save_settings():
	config.save(SAVE_PATH)

func _ready():
	config = ConfigFile.new()
	config.load(SAVE_PATH)
	set_audio_levels()
	set_controls()