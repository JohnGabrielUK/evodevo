extends Node

const SCREEN_WIDTH = 180
const SCREEN_HEIGHT = 240

const MODE_NORMAL = 1
const MODE_FIXED = 4
const MODE_SANDBOX = 5

var MODES = {
	MODE_NORMAL: {
		"name": "Evolution",
		"description": "",
		"details": "Earn points by clearing phases to spend on new body parts. (This is definitely how evolution works.)"
	},
	MODE_FIXED: {
		"name": "Fixed",
		"description": "",
		"details": "Choose all your body parts at the start of the game."
	},
	MODE_SANDBOX: {
		"name": "Sandbox",
		"description": "",
		"details": "Play the game with all parts unlocked from the beginning! You big cheat."
	}
}

onready var current_mode = MODE_NORMAL

func get_current_mode():
	return current_mode

func set_mode(mode):
	current_mode = mode

func get_mode_name(mode):
	return MODES[mode]["name"]

func get_mode_details(mode):
	return MODES[mode]["details"]

var LEVELS = {
	"test1": {
		"title": "Phase I",
		"subtitle": "Another player steps\nupon the stage.",
		"time": 30,
		"bgm": "res://sounds/ambience/intro.ogg",
		"next": "level1",
		"checkpoint": "test1",
		"scene": "res://scenes/Testbed.tscn"
	},
	"level1": {
		"title": "Phase II",
		"subtitle": "The creature encroaches on\nthe territory of another.",
		"time": 40,
		"bgm": "res://sounds/ambience/phase2.ogg",
		"next": "level2",
		"checkpoint": "test1",
		"scene": "res://scenes/levels/Level5.tscn"
	},
	"level2": {
		"title": "Phase III",
		"subtitle": "The enemy stalks from afar,\npreying upon the short-sighted.",
		"time": 30,
		"bgm": "res://sounds/ambience/level1.ogg",
		"next": "level3",
		"checkpoint": "test1",
		"scene": "res://scenes/levels/Level1.tscn"
	},
	"level3": {
		"title": "Phase IV",
		"subtitle": "The quick oft\noutlive the strong.",
		"time": 30,
		"bgm": "res://sounds/ambience/level2.ogg",
		"next": "level4",
		"checkpoint": "level1",
		"scene": "res://scenes/levels/Level2.tscn"
	},
	"level4": {
		"title": "Phase V",
		"subtitle": "The hunter need not understand\nwhat it does; only how to do it.",
		"time": 30,
		"bgm": "res://sounds/ambience/level1.ogg",
		"next": "level5",
		"checkpoint": "level2",
		"scene": "res://scenes/levels/Level4.tscn"
	},
	"level5": {
		"title": "Phase VI",
		"subtitle": "The mighty rise high.\nThe dominant push others down.",
		"time": 60,
		"bgm": "res://sounds/ambience/level3.ogg",
		"next": "the_end",
		"checkpoint": "level2",
		"scene": "res://scenes/levels/Level3.tscn"
	},
	"the_end": {
		"scene": "res://scenes/Ending.tscn"
	}
}

onready var current_level = "test1"

# Gameplay stats
onready var shots_fired = 0
onready var shots_hit = 0
onready var enemies_killed = 0
onready var times_died = 0

onready var deaths_per_level = {}

func new_game():
	for next_level in LEVELS:
		deaths_per_level[next_level] = 0
	shots_fired = 0
	shots_hit = 0
	enemies_killed = 0
	times_died = 0

func get_deaths_for_level(level):
	if not deaths_per_level.has(level): return 0 # For debugging
	return deaths_per_level[level]

func add_death_for_level(level):
	if not deaths_per_level.has(level):
		deaths_per_level[level] = 1 # For debugging
	else:
		deaths_per_level[level] += 1

func get_level_title(level):
	return LEVELS[level]["title"]

func get_level_subtitle(level):
	return LEVELS[level]["subtitle"]

func get_level_time(level):
	return LEVELS[level]["time"]

func get_checkpoint(level):
	return level

func get_next_level(level):
	return LEVELS[level]["next"]

func get_level_scene(level):
	return LEVELS[level]["scene"]

func get_level_bgm(level):
	return LEVELS[level]["bgm"]