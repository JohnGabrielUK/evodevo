extends Node2D

onready var sprite = $AnimatedSprite

var speed = 1
var size = 1
var velocity = Vector2(0, 0)

func _ready():
	sprite.speed_scale = speed
	sprite.scale.x = size
	sprite.scale.y = size
	sprite.play()

func _process(delta):
	position += velocity * delta

func _animation_finished():
	queue_free()
