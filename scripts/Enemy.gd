extends Area2D

var player # The guy we're trying to kill!
var hitpoints
var parent

var audio_hit

onready var flashing_amount = 0.0 # We flash when we get hit; this variable counts how much

func _ready():
	# Make a unique copy of the enemy material
	set_material(get_material().duplicate(true))

func _process(delta):
	if flashing_amount > 0:
		var decrease = delta * 10
		if flashing_amount > decrease:
			flashing_amount -= decrease
		else:
			flashing_amount = 0
	get_material().set_shader_param("flashing", flashing_amount)
	# If we've moved offscreen, magically disappear
	if position.y > 160:
		queue_free()

func _area_entered(area):
	if area.is_in_group("player_shot"):
		area.queue_free()
		Global.shots_hit += 1
		hitpoints -= area.damage
		# Audio/visual feedback
		flashing_amount = 1.0
		audio_hit.play()
		# If we ded, die
		if hitpoints <= 0:
			Global.enemies_killed += 1
			die()
	elif area.is_in_group("player_sword"):
		hitpoints -= area.damage
		# Audio/visual feedback
		flashing_amount = 1.0
		audio_hit.play()
		# If we ded, die
		if hitpoints <= 0:
			Global.enemies_killed += 1
			die()

func die():
	pass
