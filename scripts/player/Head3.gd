extends Node2D

const COOLDOWN_TIME = 0.4
const DAMAGE = 1

const obj_shot = preload("res://objects/player_bits/Shot1.tscn")
const obj_blind = preload("res://objects/player_bits/Blind.tscn")

onready var cooldown = 0

onready var audio_fire = $Audio_Fire
onready var outpoint = $Outpoint

var player

func _ready():
	player.set_view_scale(0.2)
	if Options.get_use_shaders():
		var blind = obj_blind.instance()
		player.add_child(blind)

func fire():
	var shot = obj_shot.instance()
	shot.damage = DAMAGE
	shot.global_position = player.position + outpoint.position
	shot.velocity = Vector2(0, -100)
	player.parent.add_child(shot)
	cooldown = COOLDOWN_TIME
	audio_fire.play()
	Global.shots_fired += 1

func get_player_input(delta):
	if !player.is_alive(): return
	# Does the player want to fire?
	if Input.is_action_pressed("fire"):
		# Make sure we can actually fire
		if cooldown > 0: return
		fire()

func _process(delta):
	# Decrement the cooldown counter until it reaches zero
	if cooldown > delta:
		cooldown -= delta
	elif cooldown > 0:
		cooldown = 0
	get_player_input(delta)