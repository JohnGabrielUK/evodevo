extends Area2D

const obj_explosion = preload("res://objects/explosions/BigExplosion.tscn")
const obj_blind = preload("res://objects/player_bits/Blind.tscn")
const obj_pausescreen = preload("res://objects/ui/PauseScreen.tscn")

export (NodePath) var path_parent
onready var parent = get_node(path_parent)

onready var camera = $Camera2D
onready var timer_posthit = $Timer_Posthit
onready var audio_hit = $Audio_Hit
onready var ui = $UI

const STATE_ALIVE = 1
const STATE_DEAD = 0

# Links to the subobjects for each customisable part of the player
var head
var arms
var legs

onready var state = STATE_ALIVE
onready var vulnerable = true

var max_hitpoints
var hitpoints

signal dead

func create_body():
	camera.target_zoom = 0.1
	# Get our body parts - head first
	var head_object = Evolution.get_current_head_object()
	if head_object != null:
		head = head_object.instance()
		AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index("Master"), 0, false)
	else:
		# If we have no head, add audiovisual impairments
		if Options.get_use_shaders():
			var blind = obj_blind.instance()
			self.add_child(blind)
		AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index("Master"), 0, true)
	# Get our body parts
	var arms_object = Evolution.get_current_arms_object()
	if arms_object != null:
		arms = arms_object.instance()
	# Get our body parts
	var legs_object = Evolution.get_current_legs_object()
	if legs_object != null:
		legs = legs_object.instance()
	# Tell everyone you're their daddy
	for next in [head, arms, legs]:
		if next != null:
			next.player = self
			add_child(next)

func _ready():
	create_body()
	timer_posthit.connect("timeout", self, "invulnerability_timeout")
	max_hitpoints = Evolution.get_current_hitpoints()
	hitpoints = max_hitpoints

func _process(delta):
	# Yes, it's massively wasteful. It wasn't working when I put it above
	ui.set_health(hitpoints, max_hitpoints)
	if Input.is_action_just_pressed("pause"):
		var pause = obj_pausescreen.instance()
		add_child(pause)

func is_alive():
	return state == STATE_ALIVE

func die():
	# Get rid of our parts
	for next in [head, arms, legs]:
		if next != null:
			next.queue_free()
	Global.times_died += 1
	var explosion = obj_explosion.instance()
	explosion.global_position = position
	get_parent().add_child(explosion)
	emit_signal("dead")
	state = STATE_DEAD
	hide()

func damage(amount):
	if !vulnerable: return
	if state == STATE_DEAD: return
	var shake_amount = min(amount, hitpoints)
	parent.shake_camera(shake_amount / 2.0)
	audio_hit.play()
	hitpoints -= amount
	ui.set_health(hitpoints, max_hitpoints)
	if hitpoints <= 0:
		die()
	else:
		vulnerable = false
		timer_posthit.start()

func invulnerability_timeout():
	vulnerable = true

# To be used by heads
func set_view_scale(amount):
	camera.target_zoom = amount
	
func _area_entered(area):
	if !area.is_in_group("player_shot") && !area.is_in_group("player_sword"): # TODO: refactor this bollocks
		damage(area.damage)

func _body_entered(body):
	damage(body.damage)
