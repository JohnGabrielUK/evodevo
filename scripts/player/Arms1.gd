extends Node2D

const COOLDOWN_TIME = 0.25
const DAMAGE = 1

const obj_shot = preload("res://objects/player_bits/Shot1.tscn")

onready var cooldown = 0

onready var audio_fire = $Audio_Fire

onready var outpoints = [$Position2D_ShotLeft, $Position2D_ShotRight]
onready var next_outpoint = 0

var player # The thing we're connected to!

func fire():
	var shot = obj_shot.instance()
	shot.damage = DAMAGE
	shot.global_position = player.position + outpoints[next_outpoint % 2].position
	shot.velocity = Vector2(0, -100)
	player.parent.add_child(shot)
	next_outpoint += 1
	cooldown = COOLDOWN_TIME
	audio_fire.play()
	Global.shots_fired += 1

func get_player_input(delta):
	if !player.is_alive(): return
	# Does the player want to fire?
	if Input.is_action_pressed("fire"):
		# Make sure we can actually fire
		if cooldown > 0: return
		fire()

func _process(delta):
	# Decrement the cooldown counter until it reaches zero
	if cooldown > delta:
		cooldown -= delta
	elif cooldown > 0:
		cooldown = 0
	get_player_input(delta)