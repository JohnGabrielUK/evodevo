extends Node2D

const obj_blurry = preload("res://objects/player_bits/Blurry.tscn")

var player

func _ready():
	player.set_view_scale(0.166)
	if Options.get_use_shaders():
		var blurry = obj_blurry.instance()
		player.add_child(blurry)