extends Node2D

const COOLDOWN_TIME = 0.4
const DAMAGE = 2
const DAMAGE_CHARGED = 8
const CHARGE_RATE = 1/1.3

const obj_shot = preload("res://objects/player_bits/Shot2.tscn")

onready var cooldown = 0
onready var charge_amount = 0

onready var audio_fire = $Audio_Fire
onready var audio_fire_charged = $Audio_FireCharged
onready var audio_charge = $Audio_Charge

onready var outpoint = $Position2D_Outpoint

var player # The thing we're connected to!

func fire_charged():
	for next_dir in [-1, 1]:
		var shot = obj_shot.instance()
		player.parent.add_child(shot)
		shot.damage = DAMAGE_CHARGED
		shot.global_position = player.position
		shot.global_position.y += outpoint.position.y
		shot.global_position.x += outpoint.position.x * next_dir
		shot.velocity = Vector2(100*next_dir, -100)
		shot.set_size(2)
		if next_dir == -1: shot.flip(true)
	cooldown = COOLDOWN_TIME
	audio_fire_charged.play()
	Global.shots_fired += 2
	player.parent.shake_camera(0.5)

func fire():
	for next_dir in [-1, 1]:
		var shot = obj_shot.instance()
		player.parent.add_child(shot)
		shot.damage = DAMAGE
		shot.global_position = player.position
		shot.global_position.y += outpoint.position.y
		shot.global_position.x += outpoint.position.x * next_dir
		shot.velocity = Vector2(100*next_dir, -100)
		if next_dir == -1: shot.flip(true)
	cooldown = COOLDOWN_TIME
	audio_fire.play()
	Global.shots_fired += 2

func get_player_input(delta):
	if !player.is_alive(): return
	# Does the player want to fire?
	if Input.is_action_pressed("fire"):
		if audio_charge.playing == false && charge_amount < 0.1:
			audio_charge.play()
		charge_amount = min(charge_amount + (CHARGE_RATE*delta), 1)
	else:
		audio_charge.stop()
		if charge_amount > 0.95:
			fire_charged()
		elif charge_amount > 0.05:
			fire()
		charge_amount = 0

func _process(delta):
	# Decrement the cooldown counter until it reaches zero
	if cooldown > delta:
		cooldown -= delta
	elif cooldown > 0:
		cooldown = 0
	get_player_input(delta)