extends Node2D

const ACCEL_SPEED = 1.5 # Speed is measured between 0 and 1; the const below is a coefficient
const MOVE_SPEED = 50
const DRAG = 0.5 # Speed at which movement is constantly slowing down

onready var h_speed = 0
onready var v_speed = 0

onready var sprite = $AnimatedSprite

var player # The thing we're connected to!

# This may or may not be outer space, but there's still drag, because videogames
func drag(delta):
	var drag_amount = DRAG * delta
	if h_speed > drag_amount:
		h_speed -= drag_amount
	elif h_speed < -drag_amount:
		h_speed += drag_amount
	else:
		h_speed = 0
	if v_speed > drag_amount:
		v_speed -= drag_amount
	elif v_speed < -drag_amount:
		v_speed += drag_amount
	else:
		v_speed = 0

func get_player_input(delta):
	# Get the player's input
	var moving = false
	if Input.is_action_pressed("move_left"):
		h_speed -= ACCEL_SPEED * delta
		moving = true
	if Input.is_action_pressed("move_right"):
		h_speed += ACCEL_SPEED * delta
		moving = true
	if Input.is_action_pressed("move_up"):
		v_speed -= ACCEL_SPEED * delta
		moving = true
	if Input.is_action_pressed("move_down"):
		v_speed += ACCEL_SPEED * delta
		moving = true
	# Keep the player's speed within limits
	h_speed = clamp(h_speed, -1, 1)
	v_speed = clamp(v_speed, -1, 1)
	# Make the tail wiggle!
	var target_wiggle = 0
	if moving:
		target_wiggle = 1
	sprite.speed_scale = lerp(sprite.speed_scale, target_wiggle, delta)

func move(delta):
	if player.is_alive():
		# Move the player
		player.position.x += h_speed * MOVE_SPEED * delta
		player.position.y += v_speed * MOVE_SPEED * delta

func screen_wrap():
	if player.position.x < -Global.SCREEN_WIDTH/2:
		player.position.x = -Global.SCREEN_WIDTH/2
		h_speed = 0
	if player.position.x > Global.SCREEN_WIDTH/2:
		player.position.x = Global.SCREEN_WIDTH/2
		h_speed = 0
	if player.position.y < -Global.SCREEN_HEIGHT/2:
		player.position.y = -Global.SCREEN_HEIGHT/2
		v_speed = 0
	if player.position.y > Global.SCREEN_HEIGHT/2:
		player.position.y = Global.SCREEN_HEIGHT/2
		v_speed = 0

func _process(delta):
	drag(delta)
	get_player_input(delta)
	move(delta)
	screen_wrap()
