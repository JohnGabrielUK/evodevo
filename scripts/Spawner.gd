extends Node2D

export (int, "Asteroid", "Sider", "Turner", "Biggie", "Spinner", "Watcher") var spawnee # The object we're going to spawn
export (NodePath) var path_gamemaster # Which class should we parent the newly spawned enemies to?
export var count = 3 # How many enemies to spawn
export var time_before = 5.0 # The time before enemies start spawning
export var time_between = 1.0 # The delay between each enemy spawning
export var distribution = -1 # How far from the spawner's x-axis should the objects be spawned? If -1, they will spawn on its exact position

var timer_start
var timer_next
var obj_spawnees

onready var spawned = 0 # How many we've spawned so far
onready var gamemaster = get_node(path_gamemaster)

func _ready():
	# Decide what object we're going to be spawning
	match spawnee:
		0: obj_spawnees = [
			load("res://objects/enemies/Asteroid1.tscn"),
			load("res://objects/enemies/Asteroid2.tscn"),
			load("res://objects/enemies/Asteroid3.tscn")
		]
		1: obj_spawnees = [load("res://objects/enemies/Sider.tscn")]
		2: obj_spawnees = [load("res://objects/enemies/Turner.tscn")]
		3: obj_spawnees = [load("res://objects/enemies/Biggie.tscn")]
		4: obj_spawnees = [load("res://objects/enemies/Spinner.tscn")]
		5: obj_spawnees = [load("res://objects/enemies/Watcher.tscn")]
	# Set the timer
	timer_start = Timer.new()
	timer_start.connect("timeout", self, "start")
	timer_start.wait_time = time_before
	timer_start.start()
	self.add_child(timer_start)

func start():
	# Set the timer
	timer_next = Timer.new()
	timer_next.connect("timeout", self, "spawn")
	timer_next.wait_time = time_between
	timer_next.start()
	self.add_child(timer_next)
	# Spawn the first enemy
	spawn()
	timer_start.stop()

func spawn():
	# Spawn the next enemy
	var next_enemy = obj_spawnees[randi() % obj_spawnees.size()].instance()
	next_enemy.position = position
	next_enemy.player = gamemaster.player
	next_enemy.parent = gamemaster
	if distribution != -1:
		var dist_x = (randf() - 0.5) * 2 * distribution
		next_enemy.position.x += dist_x
	gamemaster.add_child(next_enemy)
	# If we've spawned all the enemies we're supposed to, we're done
	spawned += 1
	if spawned >= count:
		queue_free()