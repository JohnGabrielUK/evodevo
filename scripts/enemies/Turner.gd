extends "res://scripts/Enemy.gd"

const obj_explosion = preload("res://objects/explosions/SmallExplosion.tscn")

const TURN_SPEED = 150
const MOVE_SPEED = 100

onready var sprite = $AnimatedSprite
onready var audio_turn = $Audio_Turn

onready var direction = 270
var turning_direction

const STATE_DIVING = 0
const STATE_TURN = 1

onready var state = STATE_DIVING
onready var motion_counter = 0

onready var damage = 1

func _ready():
	._ready()
	# Set up parent variables
	audio_hit = $Audio_Hit
	hitpoints = 2
	# Do our own thing
	if global_position.x < -10:
		turning_direction = -1
	elif global_position.x > 10:
		turning_direction = 1
	elif randf() > 0.5:
		turning_direction = -1
	else:
		turning_direction = 1

# SQUADRON FORTY...
func dive(delta):
	position.y += MOVE_SPEED * delta
	var x_diff = player.position.x - position.x
	var move_amount = MOVE_SPEED * delta * 0.1
	if x_diff > move_amount:
		position.x += move_amount
	elif x_diff < -move_amount:
		position.x -= move_amount
	# Get ready to turn
	motion_counter += delta
	if motion_counter > 1.5:
		state = STATE_TURN
		motion_counter = 0
		audio_turn.play()

func turn(delta):
	direction += TURN_SPEED * delta * turning_direction
	if direction > 360: direction -= 360
	if direction < 0: direction += 360
	position.x += cos(deg2rad(direction)) * MOVE_SPEED * delta
	position.y -= sin(deg2rad(direction)) * MOVE_SPEED * delta
	# Rotate in the direction we're facing
	var direction_index = int(round(direction/45)) % 8
	sprite.animation = "dir" + str(direction_index)
	# Get ready to dive
	motion_counter += TURN_SPEED * delta
	if motion_counter > 360:
		state = STATE_DIVING
		motion_counter = 0

func _process(delta):
	._process(delta) # Call parent function
	match state:
		STATE_DIVING: dive(delta)
		STATE_TURN: turn(delta)
	if position.y > 200:
		queue_free()

func die():
	var explosion = obj_explosion.instance()
	explosion.global_position = position
	get_parent().add_child(explosion)
	queue_free()
