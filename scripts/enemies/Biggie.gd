extends "res://scripts/Enemy.gd"

const obj_enemyshot = preload("res://objects/enemies/EnemyShot1.tscn")
const obj_explosion = preload("res://objects/explosions/BigExplosion.tscn")

onready var outpoint = $Position2D_Outpoint
onready var sound_drone = $Audio_Drone
onready var sound_fire = $Audio_Fire

onready var damage = 1

var timer_fire

func _ready():
	._ready()
	# Set up parent variables
	audio_hit = $Audio_Hit
	hitpoints = 10
	# Do our own thing
	timer_fire = Timer.new()
	timer_fire.connect("timeout", self, "fire")
	timer_fire.wait_time = 4
	timer_fire.start()
	add_child(timer_fire)
	sound_drone.play()

func _process(delta):
	._process(delta) # Call parent function
	position.y += delta * 15
	if position.y > 200:
		queue_free()

func fire():
	for next_dir in [-1, 1]:
		# Bite me, it works
		for next_pair in [[0, Vector2(5, 25)], [2, Vector2(10, 20)], [4, Vector2(15, 15)]]:
			var shot = obj_enemyshot.instance()
			shot.global_position = Vector2(position.x + ((outpoint.position.x + next_pair[0]) * next_dir), position.y + outpoint.position.y)
			shot.velocity = Vector2(next_pair[1].x * next_dir, next_pair[1].y)
			shot.damage = 1
			get_parent().add_child(shot)
		sound_fire.play()

func die():
	var explosion = obj_explosion.instance()
	explosion.global_position = position
	get_parent().add_child(explosion)
	parent.shake_camera(1)
	queue_free()