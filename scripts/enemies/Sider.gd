extends "res://scripts/Enemy.gd"

const obj_explosion = preload("res://objects/explosions/SmallExplosion.tscn")

onready var sprite = $AnimatedSprite

const TURN_AMOUNT = 25
const TURN_SPEED = 2.5

onready var turning_center = position.x
onready var turning_index = 0

const STATE_NORMAL = 0
const STATE_DIVING = 1

const SPEED_NORMAL = 50
const DIVE_SPEED = 200
const DIVE_ACCEL = 170

onready var state = STATE_NORMAL

onready var speed = SPEED_NORMAL

onready var damage = 2

func _ready():
	._ready()
	# Set up parent variables
	audio_hit = $Audio_Hit
	hitpoints = 3

func normal_behaviour(delta):
	position.y += delta * 30
	# Now turn around a center point
	turning_index += delta * TURN_SPEED
	position.x = turning_center + (cos(turning_index) * TURN_AMOUNT)
	# Make the ship tilt
	var tilt = sin(turning_index)
	if abs(tilt) > 0.8:
		sprite.animation = "turn2"
	elif abs(tilt) > 0.4:
		sprite.animation = "turn1"
	else:
		sprite.animation = "normal"
	if tilt > 0:
		sprite.flip_h = true
	else:
		sprite.flip_h = false
	# Has the player wandered into our divepath?
	if player != null:
		var dist_x = abs(player.position.x - position.x)
		var dist_y = player.position.y - position.y
		if dist_x < 10 && dist_y < 200 && abs(tilt) < 0.4:
			state = STATE_DIVING

func die():
	var explosion = obj_explosion.instance()
	explosion.global_position = position
	get_parent().add_child(explosion)
	queue_free()

# SQUADRON FORTY...
func dive(delta):
	if speed < DIVE_SPEED:
		speed += DIVE_ACCEL * delta
	position.y += speed * delta

func _process(delta):
	._process(delta) # Call parent function
	match state:
		STATE_NORMAL: normal_behaviour(delta)
		STATE_DIVING: dive(delta)
	if position.y > 200:
		queue_free()
