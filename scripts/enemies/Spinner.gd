extends "res://scripts/Enemy.gd"

const obj_enemyshot = preload("res://objects/enemies/EnemyShot1.tscn")
const obj_explosion = preload("res://objects/explosions/SmallExplosion.tscn")

const SHOT_OFFSET = 6
const SHOT_SPEED = 35
const DIVE_SPEED = 100

onready var sprite = $AnimatedSprite
onready var audio_fire = $Audio_Fire
onready var timer = $Timer

const STATE_DIVING = 0
const STATE_SHOOTING = 1
const MAX_SPINS = 2

var spins

onready var state = STATE_DIVING

onready var damage = 1

func _ready():
	._ready()
	# Set up parent variables
	audio_hit = $Audio_Hit
	hitpoints = 4
	sprite.connect("frame_changed", self, "fire")
	sprite.connect("animation_finished", self, "spin_finished")
	timer.connect("timeout", self, "start_firing")
	timer.start()

func start_firing():
	state = STATE_SHOOTING
	sprite.animation = "spin"
	spins = MAX_SPINS

func spin_finished():
	spins -= 1
	if spins <= 0:
		state = STATE_DIVING
		sprite.animation = "dive"
		timer.start()

func _process(delta):
	if state == STATE_DIVING:
		position.y += DIVE_SPEED * delta

func fire():
	if state == STATE_DIVING: return
	var direction = deg2rad((sprite.frame*22.5) - 90)
	var shot = obj_enemyshot.instance()
	shot.damage = 1
	shot.velocity = Vector2(cos(direction)*SHOT_SPEED, -sin(direction)*SHOT_SPEED)
	shot.position = Vector2(global_position.x + cos(direction)*SHOT_OFFSET, global_position.y - sin(direction)*SHOT_OFFSET)
	get_parent().add_child(shot)
	audio_fire.play()

func die():
	var explosion = obj_explosion.instance()
	explosion.global_position = position
	get_parent().add_child(explosion)
	queue_free()
