extends "res://scripts/Enemy.gd"

const MOVE_SPEED = 50.0

onready var sprite_eyeball = $Sprite_Eyeball
onready var sprite_eyelid = $AnimatedSprite_Eyelid
onready var audio_watching = $Audio_Watching
onready var timer_leave = $Timer_Leave

const STATE_CLOSED = 0
const STATE_OPEN = 1
const STATE_LEAVING = 2

onready var state = STATE_CLOSED
onready var velocity = Vector2(0.0, 0.0)

onready var damage = 0

func _ready():
	._ready()
	# Set up parent variables
	audio_hit = $Audio_Hit
	hitpoints = 9001 # You ain't killing this one, player

func _process(delta):
	._process(delta) # Call parent function
	position.y += delta * 20
	# Move towards the player
	var relative_x = player.position.x - position.x
	var relative_y = player.position.y - position.y
	var distance_to_player = sqrt(pow(relative_x, 2.0) + pow(relative_y, 2.0))
	var vector_to_player = Vector2(relative_x, relative_y).normalized()
	match state:
		STATE_CLOSED:
			position.x = lerp(position.x, player.position.x, 0.05)
			if distance_to_player < 25:
				sprite_eyelid.animation = "open"
				audio_watching.play()
				state = STATE_OPEN
				timer_leave.start()
		STATE_OPEN:
			position.y = lerp(position.y, player.position.y - 25, 0.05)
			if relative_x > 0: velocity.x = min(velocity.x + delta, 1.0)
			elif relative_x < 0: velocity.x = max(velocity.x - delta, -1.0)
			position.x += velocity.x * MOVE_SPEED * delta
		STATE_LEAVING:
			velocity.y -= delta / 2.0
			if relative_x > 0: velocity.x = min(velocity.x + delta, 1.0)
			elif relative_x < 0: velocity.x = max(velocity.x - delta, -1.0)
			position.x += velocity.x * MOVE_SPEED * delta
			position.y += velocity.y * MOVE_SPEED * delta
	# Lookit my faaaaaace
	sprite_eyeball.position.x = (vector_to_player.x*2.0) + (randf() - 0.5) / 2.0
	sprite_eyeball.position.y = (vector_to_player.y) + (randf() - 0.5) / 2.0

func _leave():
	state = STATE_LEAVING

func die():
	pass
