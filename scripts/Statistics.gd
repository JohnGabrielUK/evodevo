extends Control

const obj_transition = preload("res://objects/ui/Transition.tscn")

onready var animplayer = $AnimationPlayer

onready var label_shots = $Grid_Statistics/Label_Shots_Value
onready var label_accuracy = $Grid_Statistics/Label_Accuracy_Value
onready var label_enemieskilled = $Grid_Statistics/Label_EnemiesKilled_Value
onready var label_timesdied = $Grid_Statistics/Label_TimesDied_Value

func _ready():
	label_shots.text = "%s/%s" % [Global.shots_hit, Global.shots_fired]
	if Global.shots_fired != 0:
		var accuracy = float(Global.shots_hit) / float(Global.shots_fired)
		label_accuracy.text = str(int(accuracy * 100)) + "%"
	else:
		label_accuracy.text = "N/A"
	label_enemieskilled.text = str(Global.enemies_killed)
	label_timesdied.text = str(Global.times_died)
	animplayer.play("Statistics")
	#bgm.play()

func finish():
	var transition = obj_transition.instance()
	transition.target = "res://scenes/TitleScreen.tscn"
	self.add_child(transition)
