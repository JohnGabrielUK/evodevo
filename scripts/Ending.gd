extends Control

const obj_statistics = preload("res://scenes/Statistics.tscn")

onready var vbox = $VBox
onready var anim_player = $AnimationPlayer
onready var music = $Audio_Music

onready var paragraphs = [$VBox/Label_Paragraph1, $VBox/Label_Paragraph2, $VBox/Label_Paragraph3, $VBox/Label_Paragraph4]

var text

func select_ending():
	# Load the various endings
	var file = File.new()
	file.open("res://texts/endings.tscn", file.READ)
	var endings = parse_json(file.get_as_text())
	file.close()
	# Choose the appropriate ending to display
	if Global.shots_fired > 0:
		text = endings["violent"]
	else:
		text = endings["pacifist"]
	for i in range(0, text.size()):
		paragraphs[i].text = text[i]

func _ready():
	select_ending()
	anim_player.play("Ending")
	music.play()
	# Turn off audio impairments
	AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index("Master"), 0, false)

func _input(event):
	if event.is_action_pressed("pause"):
		anim_player.seek(53, true)

func done():
	var stats = obj_statistics.instance()
	self.add_child(stats)