extends Control

onready var vbox = $VBox
onready var anim_player = $AnimationPlayer
onready var music = $Audio_Music

onready var paragraphs = [$VBox/Label_Paragraph1, $VBox/Label_Paragraph2, $VBox/Label_Paragraph3, $VBox/Label_Paragraph4]

var text

func _ready():
	anim_player.play("Opening")
	music.play()
	# Turn off audio impairments
	AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index("Master"), 0, false)

func _input(event):
	if event.is_action_pressed("pause"):
		anim_player.seek(30.5, true)

func done():
	# This feels dirty.
	get_tree().change_scene("res://scenes/Testbed.tscn")