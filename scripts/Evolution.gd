extends Node

const HEADS = 0
const ARMS = 1
const LEGS = 2

class Configuration:
	var _head
	var _arms
	var _legs
	
	func _init(head, arms, legs):
		_head = head
		_arms = arms
		_legs = legs
	
	# Hem hem!
	func get_head():
		return _head
	
	func get_arms():
		return _arms
	
	func get_legs():
		return _legs
	
	func set_head(head):
		_head = head
	
	func set_arms(arms):
		_arms = arms
	
	func set_legs(legs):
		_legs = legs
	
	func clone():
		return Configuration.new(_head, _arms, _legs)

var PARTS = {
	HEADS: {
		0: {
			"object": null,
			"image": null,
			"offset": 0,
			"description": "You have no head, and are therefore ideal to be either the average voter or their candidate.",
			"traits": "> Narrow vision",
			"hitpoints": 0,
			"neighbours": [1]
		},
		1: {
			"object": preload("res://objects/player_bits/Head1.tscn"),
			"image": preload("res://sprites/player_bits/head1.png"),
			"offset": 7,
			"description": "A noggin aimed at the value-focused user, with a complementary eye. Who needs depth perception, anyway?",
			"traits": "> Basic vision",
			"hitpoints": 1,
			"neighbours": [0, 2]
		},
		2: {
			"object": preload("res://objects/player_bits/Head2.tscn"),
			"image": preload("res://sprites/player_bits/head2.png"),
			"offset": 8,
			"description": "Sensitive to all it perceives, such as oncoming bullets.",
			"traits": "> Extended sight\n> No hitpoint bonus",
			"hitpoints": 0,
			"neighbours": [1, 3]
		},
		3: {
			"object": preload("res://objects/player_bits/Head3.tscn"),
			"image": preload("res://sprites/player_bits/head3.png"),
			"offset": 10,
			"description": "It may not look pretty, but physical appearance tends not to matter so much when you can fire lasers from your eyeballs.",
			"traits": "> Limited vision\n> Fires projectiles",
			"hitpoints": 2,
			"neighbours": [2]
		}
	},
	ARMS: {
		0: {
			"object": null,
			"image": null,
			"offset": 0,
			"description": "You are unburdened by the complexities of dexterity.",
			"traits": "> Defenceless",
			"hitpoints": 0,
			"neighbours": [1]
		},
		1: {
			"object": preload("res://objects/player_bits/Arms1.tscn"),
			"image": preload("res://sprites/player_bits/arms1.png"),
			"offset": 3,
			"description": "A pair of arms, ideal for waving a friendly greeting by way of bullets.",
			"traits": "> Fires directly ahead",
			"hitpoints": 1,
			"neighbours": [0, 2]
		},
		2: {
			"object": preload("res://objects/player_bits/Arms2.tscn"),
			"image": preload("res://sprites/player_bits/arms2.png"),
			"offset": 0,
			"description": "All the benefit of enhanced firepower with all the challenges of it being pointed in completely the wrong direction.",
			"traits": "> Heavy firepower\n> Can charge shots\n> Fires diagonally",
			"hitpoints": 2,
			"neighbours": [1, 3]
		},
		3: {
			"object": preload("res://objects/player_bits/Arms3.tscn"),
			"image": preload("res://sprites/player_bits/arms3.png"),
			"offset": 4,
			"description": "Sometimes, you can't stop an asteroid from smacking you in the face. You can, however, make it sincerely regret doing so.",
			"traits": "> Extreme damage on contact",
			"hitpoints": 2,
			"neighbours": [2]
		}
	},
	LEGS: {
		0: {
			"object": null,
			"image": null,
			"offset": 0,
			"description": "Much like a Brightoner on a Friday night, you are legless.",
			"traits": "> Cannot move",
			"hitpoints": 0,
			"neighbours": [1]
		},
		1: {
			"object": preload("res://objects/player_bits/Legs1.tscn"),
			"image": preload("res://sprites/player_bits/legs1-1.png"),
			"offset": -6,
			"description": "The maneuveurability of a fin allows for such luxuries as not being smacked in the face by passing rocks.",
			"traits": "> Can move in both directions",
			"hitpoints": 1,
			"neighbours": [0, 2]
		},
		2: {
			"object": preload("res://objects/player_bits/Legs2.tscn"),
			"image": preload("res://sprites/player_bits/legs2-1.png"),
			"offset": -6,
			"description": "A pair of sturdy legs, with the added benefit of being able to casually disregard Euclidian geometry.",
			"traits": "> Faster horizontal movement\n> Slower vertical movement\n> Screen wraps horizontally",
			"hitpoints": 0,
			"neighbours": [1, 3]
		},
		3: {
			"object": preload("res://objects/player_bits/Legs3.tscn"),
			"image": preload("res://sprites/player_bits/legs3.png"),
			"offset": -10,
			"description": "Postmen across the galaxy will attest that being encumbered by heavy trousers is acceptable as long as they protect you from interstellar border collies.",
			"traits": "> Slow movement\n> Heavy defence",
			"hitpoints": 3,
			"neighbours": [2]
		}
	}
}

onready var current_config = Configuration.new(2, 1, 1)

# Used by normal/random mode
onready var previous_config = Configuration.new(0, 0, 0)

onready var evolution_points = 4

# Used by hard mode
onready var level_configs = {}

func get_current_head_object():
	var current_head = current_config.get_head()
	return PARTS[HEADS][current_head]["object"]

func get_current_arms_object():
	var current_arms = current_config.get_arms()
	return PARTS[ARMS][current_arms]["object"]

func get_current_legs_object():
	var current_legs = current_config.get_legs()
	return PARTS[LEGS][current_legs]["object"]

func get_part_image(type, index):
	return PARTS[type][index]["image"]

func get_current_part(type):
	match type:
		HEADS: return current_config.get_head()
		ARMS: return current_config.get_arms()
		LEGS: return current_config.get_legs()
	# We should not have got here
	print("get_current_part called with invalid body type: %s" % type)
	return null

func is_part_selectable(type, index):
	#var current = PARTS[type][get_current_part(type)]
	#return index in current["neighbours"]
	var cost = index - get_current_part(type) 
	if cost <= get_evolution_points_to_spend():
		return true

func get_part_sprite_offset(type, index):
	return PARTS[type][index]["offset"]

func get_part_description(type, index):
	return PARTS[type][index]["description"] 

func get_part_traits(type, index):
	return PARTS[type][index]["traits"]

func get_part_hitpoints(type, index):
	return PARTS[type][index]["hitpoints"]

func set_current_part(type, index):
	match type:
		HEADS: current_config.set_head(index)
		ARMS: current_config.set_arms(index)
		LEGS: current_config.set_legs(index)

func get_current_hitpoints():
	return 1 + get_part_hitpoints(HEADS, current_config.get_head()) + get_part_hitpoints(ARMS, current_config.get_arms()) + get_part_hitpoints(LEGS, current_config.get_legs())

func new_game():
	current_config = Configuration.new(0, 0, 0)
	previous_config = Configuration.new(0, 0, 0)
	evolution_points = 1

# This function is called when we complete a level
func progress_evolution():
	if Global.get_current_mode() == Global.MODE_NORMAL:
		previous_config = current_config.clone()

# This function is called when we lose a life
func reverse_evolution():
	if Global.get_current_mode() == Global.MODE_NORMAL:
		current_config = previous_config.clone()

func get_evolution_points_spent():
	return current_config.get_head() + current_config.get_arms() + current_config.get_legs()

func get_evolution_points_to_spend():
	return evolution_points - get_evolution_points_spent()