extends Node2D

const obj_phaseintro = preload("res://objects/ui/PhaseIntro.tscn")
const obj_phasecomplete = preload("res://objects/ui/PhaseComplete.tscn")
const obj_gameover = preload("res://objects/ui/GameOver.tscn")

export (String) var level

export (NodePath) var path_player
onready var player = get_node(path_player)

export (NodePath) var path_starfield
onready var starfield = get_node(path_starfield)

var timer
var bgm_player

func setup_intro():
	var phaseintro = obj_phaseintro.instance()
	phaseintro.title = Global.get_level_title(level)
	phaseintro.subtitle = Global.get_level_subtitle(level)
	phaseintro.connect("begin_level", self, "begin_level")
	self.add_child(phaseintro)

func setup_bgm():
	bgm_player = AudioStreamPlayer.new()
	bgm_player.stream = load(Global.get_level_bgm(level))
	bgm_player.bus = "BGM"
	self.add_child(bgm_player)

func setup_end():
	var time = Global.get_level_time(level)
	if time == -1: return
	timer = Timer.new()
	timer.wait_time = time
	timer.connect("timeout", self, "level_complete")
	timer.start()
	self.add_child(timer)

func shake_camera(amount):
	player.camera.shake += amount

func begin_level():
	bgm_player.play()
	starfield.pause_mode = Node.PAUSE_MODE_STOP

func level_complete():
	bgm_player.stop()
	# Let the evolution singleton know that we've cleared the level
	Evolution.progress_evolution()
	# Do the flashy things
	var phasecomplete = obj_phasecomplete.instance()
	if Global.get_deaths_for_level(Global.current_level) == 0 && Global.get_current_mode() == Global.MODE_NORMAL:
		phasecomplete.perfect = true
		Evolution.evolution_points += 2
	else:
		phasecomplete.perfect = false
		Evolution.evolution_points += 1
	phasecomplete.connect("done", self, "next_level")
	self.add_child(phasecomplete)

func next_level():
	var next = Global.get_next_level(level)
	var scene = Global.get_level_scene(next)
	get_tree().change_scene(scene)

func game_over():
	bgm_player.stop()
	if timer != null: timer.stop()
	# Depending on what gamemode we're playing, we may need to change our evolution
	Evolution.reverse_evolution()
	Global.add_death_for_level(Global.current_level)
	# Set up the gameover thingy
	var gameover = obj_gameover.instance()
	if Global.get_current_mode() == Global.MODE_FIXED:
		gameover.target = Global.get_level_scene(Global.current_level)
	else:
		gameover.target = "res://scenes/PartSelector.tscn"
	self.add_child(gameover)

func _ready():
	player.connect("dead", self, "game_over")
	Global.current_level = level
	setup_intro()
	setup_bgm()
	setup_end()