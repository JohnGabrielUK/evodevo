extends Node2D

const obj_explosion1 = preload("res://objects/explosions/Explosion1.tscn")
const obj_explosion2 = preload("res://objects/explosions/Explosion2.tscn")

onready var audio_explosion = $Audio_Explosion
onready var particles = $Particles2D

func _ready():
#	for i in range(0, 4):
#		var explosion = obj_explosion1.instance()
#		explosion.rotation_degrees = i * 90
#		explosion.size = 1
#		add_child(explosion)
	var explosion = obj_explosion2.instance()
	explosion.size = 0.5
	explosion.velocity = Vector2(0, -10)
	add_child(explosion)
	#particles.emitting = true
	audio_explosion.play()