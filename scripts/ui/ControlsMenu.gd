extends Control

const obj_options = preload("res://objects/ui/OptionsMenu.tscn")
const obj_button = preload("res://objects/ui/RebindControl.tscn")

onready var label_prompt = $Label_Prompt
onready var vbox_controls = $VBox_Controls

onready var audio_mouseover = $Audio_Mouseover
onready var audio_selected = $Audio_Selected

const actions = {
	"move_up": "Move up",
	"move_down": "Move down",
	"move_left": "Move left",
	"move_right": "Move right",
	"fire": "Fire",
	"pause": "Pause/skip"
}

onready var buttons = {}
onready var currently_binding = false

var binding_action
var parent

func setup_buttons():
	for next_action in actions:
		var scancode = Options.get_key_binding(next_action)
		# Create the button
		var button = obj_button.instance()
		vbox_controls.add_child(button)
		button.set_label_name(actions[next_action])
		button.set_label_binding(OS.get_scancode_string(scancode))
		button.action = next_action
		button.connect("rebind", self, "start_rebinding")
		buttons[next_action] = button

func start_rebinding(action):
	audio_selected.play()
	label_prompt.text = "Press key for '%s'; ESC to cancel." % actions[action]
	binding_action = action
	currently_binding = true

func rebind_action(action, key):
	# Hang on; make sure the ESC key wasn't just pressed - if it was, the user doesn't want to rebind anymore
	if not key.is_action("ui_cancel"):
		# Find the button that corresponds to this control, and update it
		var button = buttons[action]
		button.set_label_binding(key.as_text())
		# Update the Options singleton
		Options.set_key_binding(action, key.scancode)
	# We're done; stop rebinding keys until the user wants to rebind another action
	currently_binding = false
	label_prompt.text = ""

func _input(event):
	if event is InputEventKey:
		if currently_binding:
			get_tree().set_input_as_handled()
			rebind_action(binding_action, event)

func mouseover():
	audio_mouseover.play()

func back_pressed():
	Options.save_settings()
	var options = obj_options.instance()
	options.parent = parent
	parent.add_child(options)
	queue_free()

func _ready():
	# Play the "button clicked" sound for the button that got us here
	audio_selected.play()
	setup_buttons()