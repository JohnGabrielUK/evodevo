extends Button

var mode # Which game mode this button represents

signal mouseover
signal start_game

func _mouse_entered():
	emit_signal("mouseover", mode)

func _pressed():
	emit_signal("start_game", mode)
