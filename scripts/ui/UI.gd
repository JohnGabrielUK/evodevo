extends CanvasLayer

onready var healthbar = $Healthbar

func set_health(hp, max_hp):
	if max_hp == 1:
		healthbar.hide()
		return
	healthbar.value = hp
	healthbar.max_value = max_hp
	healthbar.rect_size.x = 24 + (24 * max_hp)