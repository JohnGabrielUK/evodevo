extends Control

const obj_modebutton = preload("res://objects/ui/ModeButton.tscn")
const obj_transition = preload("res://objects/ui/Transition.tscn")

onready var vbox_modes = $Center/VBox_Modes
onready var label_description = $Label_Description
onready var audio_mouseover = $Audio_Mouseover
onready var audio_selected = $Audio_Selected

var parent

func _ready():
	for next_mode in Global.MODES:
		var button = obj_modebutton.instance()
		button.mode = next_mode
		button.text = Global.get_mode_name(next_mode)
		button.connect("mouseover", self, "mouseover")
		button.connect("mouse_exited", self, "reset_mouseover")
		button.connect("start_game", self, "start_game")
		vbox_modes.add_child(button)

func start_game(mode):
	audio_selected.play()
	parent.stopBGM()
	Evolution.new_game()
	Global.set_mode(mode)
	Global.new_game()
	var scene
	# If we're playing fixed mode, go straight to the part selector
	if mode in [Global.MODE_FIXED, Global.MODE_SANDBOX]:
		scene = "res://scenes/PartSelector.tscn"
	# Normally, we'd go straight into the game
	else:
		scene = "res://scenes/Intro.tscn"
	var transition = obj_transition.instance()
	transition.target = scene
	self.add_child(transition)

func mouseover(mode):
	audio_mouseover.play()
	var text = Global.get_mode_details(mode)
	label_description.text = text
	label_description.visible_characters = 0

func reset_mouseover():
	label_description.text = ""

func back_mouseover():
	audio_mouseover.play()
	label_description.text = ""

func back_pressed():
	parent.show_title()
	queue_free()

func type_letter():
	label_description.visible_characters += 1
