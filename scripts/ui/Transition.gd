extends CanvasLayer

onready var animplayer = $AnimationPlayer

var target

func _ready():
	get_tree().paused = true
	animplayer.play("Transition")

func finished(anim_name):
	get_tree().paused = false
	get_tree().change_scene(target)