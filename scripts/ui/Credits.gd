extends Control

onready var audio_mouseover = $Audio_Mouseover
onready var audio_selected = $Audio_Selected

var parent

func mouseover():
	audio_mouseover.play()

func back_pressed():
	Options.save_settings()
	parent.show_title()
	queue_free()