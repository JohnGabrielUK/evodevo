extends CanvasLayer

const obj_transition = preload("res://objects/ui/Transition.tscn")

onready var audio_mouseover = $Audio_Mouseover
onready var audio_selected = $Audio_Selected

onready var things_to_hide = [$Label_Paused, $HBox]

func hide_stuff():
	for next_thing in things_to_hide:
		next_thing.hide()

func _ready():
	get_tree().paused = true

func _process(delta):
	if Input.is_action_just_pressed("pause"):
		_resume()

func _resume():
	get_tree().paused = false
	queue_free()

func _retry():
	audio_selected.play()
	hide_stuff()
	var transition = obj_transition.instance()
	transition.target = Global.get_level_scene(Global.current_level)
	self.add_child(transition)

func _exit():
	audio_selected.play()
	hide_stuff()
	var transition = obj_transition.instance()
	transition.target = "res://scenes/TitleScreen.tscn"
	self.add_child(transition)

func _mouseover():
	audio_mouseover.play()
