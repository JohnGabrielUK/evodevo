extends Control

const obj_icon = preload("res://objects/ui/PartIcon.tscn")
const obj_transition = preload("res://objects/ui/Transition.tscn")

onready var hbox_heads = $Center/VBox_Icons/HBox_Heads
onready var hbox_arms = $Center/VBox_Icons/HBox_Arms
onready var hbox_legs = $Center/VBox_Icons/HBox_Legs

onready var boxes = [hbox_heads, hbox_arms, hbox_legs]

onready var icons = []

onready var label_description = $VBox_Text/Label_Description
onready var label_traits = $VBox_Text/Label_Traits

onready var button_done = $VBox_Text/HBox/Button_Done
onready var label_points = $VBox_Text/HBox/Label_Points

onready var audio_bgm = $Audio_BGM
onready var audio_selected = $Audio_Selected
onready var videoplayer = $VideoPlayer

func make_box(type, index):
	var new_icon = obj_icon.instance()
	new_icon.type = type
	new_icon.index = index
	new_icon.parent = self
	boxes[type].add_child(new_icon)
	icons.append(new_icon)

func init_boxes():
	for i in range(0, 3):
		for j in range(0, 4):
			make_box(i, j)

func init_typer():
	var timer = Timer.new()
	timer.wait_time = 0.02
	timer.connect("timeout", self, "type")
	timer.start()
	self.add_child(timer)

func type():
	for i in range(0, 2):
		if label_description.percent_visible < 1:
			label_description.visible_characters += 1
		elif label_traits.percent_visible < 1:
			label_traits.visible_characters += 1

func _ready():
	init_boxes()
	init_typer()
	refresh_point_counter()
	videoplayer.play()
	audio_bgm.play()
	# If we're in fixed mode, show the done button
	if Global.get_current_mode() in [Global.MODE_FIXED, Global.MODE_SANDBOX]:
		button_done.show()
	# Turn off audio impairments
	AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index("Master"), 0, false)

func set_text(description, traits):
	label_description.visible_characters = 0
	label_traits.visible_characters = 0
	label_description.text = description
	label_traits.text = traits

func reset_text():
	label_description.text = ""
	label_traits.text = ""
	label_description.percent_visible = 1
	label_traits.percent_visible = 1

func refresh_point_counter():
	if Global.current_mode != Global.MODE_NORMAL:
		label_points.text = ""
		return
	var points = Evolution.get_evolution_points_to_spend()
	if points == 1: 
		label_points.text = "1 point to spend"
	else:
		label_points.text = "%s points to spend" % points

func part_selected():
	audio_selected.play()
	for next in icons: next.refresh()
	refresh_point_counter()

func done():
	audio_selected.play()
	audio_bgm.stop()
	var next = Global.get_checkpoint(Global.current_level)
	var scene = Global.get_level_scene(next)
	var transition = obj_transition.instance()
	transition.target = scene
	self.add_child(transition)

func videoPlayer_finished():
	videoplayer.play()
