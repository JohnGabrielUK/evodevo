extends CanvasLayer

onready var label_title = $VBox/Label_Title
onready var label_subtitle = $VBox/Label_Subtitle
onready var animplayer = $AnimationPlayer

var title
var subtitle

signal begin_level

func _ready():
	label_title.text = title
	label_subtitle.text = subtitle
	get_tree().paused = true
	animplayer.play("PhaseIntro")

func begin_level():
	get_tree().paused = false
	emit_signal("begin_level")

func finished(anim_name):
	queue_free()