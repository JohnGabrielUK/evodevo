extends Control

const obj_modeselector = preload("res://objects/ui/ModeSelector.tscn")
const obj_optionsmenu = preload("res://objects/ui/OptionsMenu.tscn")
const obj_credits = preload("res://objects/ui/Credits.tscn")

onready var animplayer = $AnimationPlayer
onready var videoplayer = $VideoPlayer
onready var audio_ambience = $Audio_Ambience
onready var audio_thud = $Audio_Thud
onready var audio_start = $Audio_Start
onready var audio_mouseover = $Audio_Mouseover

# The objects to hide when showing options/gamemode menus
onready var stuff_to_hide = [$Texture_Logo, $VBox_Buttons, $Label_ByLine, $Label_VersionNumber]

func _ready():
	animplayer.play("Intro")
	videoplayer.play()
	audio_ambience.play()
	# Turn off audio impairments
	AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index("Master"), 0, false)

func play():
	audio_start.play()
	hide_title()
	# Add the menu
	var mode_selector = obj_modeselector.instance()
	mode_selector.parent = self
	self.add_child(mode_selector)

func _input(event):
	if event.is_action_pressed("pause"):
		animplayer.seek(18, true)

func videoplayer_finished():
	videoplayer.play()

func mouseover():
	audio_mouseover.play()

func wantToQuit():
	get_tree().quit()

func stopBGM():
	audio_ambience.stop()

func hide_title():
	for next_thing in stuff_to_hide:
		next_thing.hide()

func show_title():
	audio_start.play()
	for next_thing in stuff_to_hide:
		next_thing.show()

func show_options():
	audio_start.play()
	hide_title()
	# Add the menu
	var options = obj_optionsmenu.instance()
	options.parent = self
	self.add_child(options)

func show_credits():
	audio_start.play()
	hide_title()
	# Add the menu
	var credits = obj_credits.instance()
	credits.parent = self
	self.add_child(credits)
