extends CanvasLayer

onready var animplayer = $AnimationPlayer

var target

func _ready():
	animplayer.play("GameOver")

func finished(anim_name):
	get_tree().change_scene(target)