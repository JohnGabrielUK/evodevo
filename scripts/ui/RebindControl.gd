extends HBoxContainer

onready var label_name = $Label_ControlName
onready var label_binding = $Label_CurrentBinding
onready var button_rebind = $Button_Rebind
onready var audio_mouseover = $Audio_Mouseover

var action

signal rebind

func set_label_name(text):
	label_name.text = text

func set_label_binding(text):
	label_binding.text = text

func _ready():
	pass # Replace with function body.

func _rebind():
	emit_signal("rebind", action)

func _mouseover():
	audio_mouseover.play()
