extends Control

onready var sprite = $Sprite_Part
onready var core = $Sprite_Core
onready var circle = $Sprite_Circle

onready var audio_mouseover = $Audio_Mouseover

export (int, "Head", "Arms", "Legs") var type = 1
export (int) var index = 1

onready var mouse_over = false

var parent

func _ready():
	refresh()

func is_selectable():
	# If we're in fixed or sandbox mode, everything can be selected
	if Global.get_current_mode() in [Global.MODE_FIXED, Global.MODE_SANDBOX]:
		return true
	# Otherwise; do we have the points to spend on this?
	else:
		return Evolution.is_part_selectable(type, index)

func refresh():
	sprite.texture = Evolution.get_part_image(type, index)
	sprite.offset.y = Evolution.get_part_sprite_offset(type, index)
	# If the part is selected, the circle should be bright white
	if Evolution.get_current_part(type) == index:
		circle.modulate = Color("52a892")
		sprite.modulate = Color(1.0, 1.0, 1.0)
	elif is_selectable():
		circle.modulate = Color("07382b")
		sprite.modulate = Color(0.5, 0.5, 0.5)
	else:
		circle.modulate = Color("1d2825")
		core.modulate = Color(0.25, 0.25, 0.25)
		sprite.modulate = Color(0.1, 0.1, 0.1)
	circle.modulate.a = 0.5

func _input(event):
	if event is InputEventMouseButton && mouse_over && event.is_pressed():
		# We've been clicked! Should we respond?
		if is_selectable():
			Evolution.set_current_part(type, index)
			parent.part_selected()

func _mouse_entered():
	parent.set_text(Evolution.get_part_description(type, index), Evolution.get_part_traits(type, index))
	if !mouse_over:
		audio_mouseover.play()
		mouse_over = true

func _mouse_exited():
	parent.reset_text()
	mouse_over = false
