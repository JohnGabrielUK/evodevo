extends CanvasLayer

onready var label_title = $Label_Title
onready var animplayer = $AnimationPlayer
onready var audio_clear = $Audio_Clear

signal done

var perfect

func _ready():
	# Turn off audio impairments
	AudioServer.set_bus_effect_enabled(AudioServer.get_bus_index("Master"), 0, false)
	get_tree().paused = true
	if perfect:
		animplayer.play("PerfectRun")
	else:
		animplayer.play("PhaseComplete")
	audio_clear.play()

func finished(anim_name):
	get_tree().paused = false
	emit_signal("done")