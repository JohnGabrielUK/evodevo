extends Control

const obj_controls = preload("res://objects/ui/ControlsMenu.tscn")

onready var slider_volume_bgm = $GridContainer/HSlider_Volume_BGM
onready var slider_volume_sfx = $GridContainer/HSlider_Volume_SFX
onready var slider_volume_ui = $GridContainer/HSlider_Volume_UI
onready var button_camera_shake = $GridContainer/CheckBox_CameraShake
onready var button_use_shaders = $GridContainer/CheckBox_Shaders

onready var audio_mouseover = $Audio_Mouseover
onready var audio_selected = $Audio_Selected

var parent

func mouseover():
	audio_mouseover.play()

func back_pressed():
	Options.save_settings()
	parent.show_title()
	queue_free()

func _volume_bgm_changed(value):
	Options.set_volume_bgm(value)

func _volume_sfx_changed(value):
	Options.set_volume_sfx(value)

func _volume_ui_changed(value):
	Options.set_volume_ui(value)

func _camera_shake_changed(button_pressed):
	Options.set_camera_shake(button_pressed)
	audio_selected.play()

func _use_shaders_changed(button_pressed):
	Options.set_use_shaders(button_pressed)
	audio_selected.play()

func _show_controls():
	Options.save_settings()
	var controls = obj_controls.instance()
	controls.parent = parent
	parent.add_child(controls)
	queue_free()

func _ready():
	# Play the "button clicked" sound for the button that got us here
	audio_selected.play()
	slider_volume_bgm.value = Options.get_volume_bgm()
	slider_volume_sfx.value = Options.get_volume_sfx()
	slider_volume_ui.value = Options.get_volume_ui()
	button_camera_shake.pressed = Options.get_camera_shake()
	button_use_shaders.pressed = Options.get_use_shaders()
