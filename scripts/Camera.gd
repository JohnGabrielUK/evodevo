extends Camera2D

const SHAKE_AMOUNT = 10

onready var shake = 0
var target_zoom

func _process(delta):
	# We zoomin'?
	zoom.x = lerp(zoom.x, target_zoom, delta/2)
	zoom.y = lerp(zoom.y, target_zoom, delta/2)
	# We shakin'?
	if shake > 0 && Options.get_camera_shake():
		# Shake it, baby
		offset.x = randf() * pow(shake, 2) * SHAKE_AMOUNT
		offset.y = randf() * pow(shake, 2) * SHAKE_AMOUNT
		# Decrement the shake variable
		if shake > delta:
			shake -= delta
		else:
			shake = 0