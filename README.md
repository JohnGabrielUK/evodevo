# Evodevo

## What?

It's a vertical shooter where you can customise your creature.

## Why?

It was made for the first Godot Wild Jam.

## How?

The game was made in Godot 3.1 alpha 1, the pixelart was done with GIMP, and the 3D background was done in Blender.

## Who?

Me! I did all the design, code, and art. The sound effects were taken from the free Sonniss libraries, and the music is by Kevin MacLeod.
> 
    "Shadowlands 4 - Breath", "Despair and Triumph"
    Kevin MacLeod (incompetech.com)
    Licensed under CC BY 3.0
    http://creativecommons.org/licenses/by/3.0/

